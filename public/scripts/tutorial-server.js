var data = [
    {id: 1, author: "Lukáš Havlíček", text: "První komentář"},
    {id: 2, author: "Igor Hnízdo", text: "Druhý komentář"}
];

var Comment = React.createClass({
    render: function() {
        return (
            <div className="comment">
                <h2 className="commentAuthor">
                    {this.props.author}
                </h2>
                {this.props.children}
            </div>
        );
    }
});

var CommentList = React.createClass({
    render: function() {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Comment author={comment.author}>{comment.text}</Comment>
            );
        });

        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});

var CommentForm = React.createClass({
    render: function() {
        return(
            <form className="commentForm">
                <input type="text" placeholder="Jméno"/>
                <input type="text" placeholder="Komentář"/>
                <input type="submit" placeholder="Odeslat"/>
            </form>
        );
    }
});

var CommentBox = React.createClass({
    loadCommentsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                // setState zpusobi prekresleni komponent
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    // Volano po prvnim vytvoreni
    getInitialState: function() {
        return {data: []};
    },
    // Volano po prvni vyrenderovani
    componentDidMount: function() {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function () {
        return (
            <div className="commentBox">
                <h1>Komentáře</h1>
                <CommentList data={this.state.data} />
                <CommentForm />
            </div>
        );
    }
});

ReactDOM.render(
    <CommentBox url="/api/comments" pollinterval={2000} />,
    document.getElementById('content')
);
